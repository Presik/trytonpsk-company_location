# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Location(ModelSQL, ModelView):
    "Company Location"
    __name__ = "company.location"
    name = fields.Char('Name', required=True, translate=True)
    parent = fields.Many2One('company.location','Parent', select=True)
    childs = fields.One2Many('company.location', 'parent', string='Children')

    @classmethod
    def __setup__(cls):
        super(Location, cls).__setup__()

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + self.name
        else:
            return self.name
